#include <Arduino.h>
#include <Servo.h>
#include <SimpleTimer.h>
#include <Wire.h>
#include "Servos.h"
#include "Behaviour.h"
#include "Idle.h"
#include "Lean.h"
#include "Walk.h"
#include "Parameters.h"

Servos servos;

SimpleTimer timer;
float period;     // cycle from 0 to 1 indefinitely
long selector;    // take a random value every period

// Behaviours
Idle idle;
Lean lean;
Walk walk;
Behaviour* behaviours[] = {&walk, &lean, &idle};    // sorted array (highest to lowest priority)
Behaviour* winner;

bool periodReset = true;

int sel=4;
float dir=0., magn=0., magnRequest=0.;

long lastMillis = 0;
float lastPeriod = 0.;
long periodStart = 0, lastTask = 0;
long circleStart = 0;
int polygon = 1, periodsPerLine=1;

int periodsElapsed = 0;

void setup() { 
    Serial.begin(BAUDRATE);
    while (true)
      if (Serial.available() > 0 && Serial.read() == 's')
        break;
    servos.attachAll();
    
    // Timed actions setup
    timer.setInterval(SPEED, markTime);
    timer.setInterval(20, subsume);
}

void loop() {
    if (Serial.available() > 0) {
      char c = Serial.read();
        if (c == 'q') {
          servos.detachAll();
          while (!(Serial.available() > 0 && Serial.read() == 's'));
          servos.attachAll();
        }
        if (c == 'm') {
          while (Serial.available() < 3);
          magnRequest = (float)n(Serial.read())*100 + n(Serial.read())*10 + n(Serial.read());
          Serial.println(magnRequest);
        }
        if (c == 'k') {
          while (Serial.available() < 3);
          dir = (float)n(Serial.read())*100 + n(Serial.read())*10 + n(Serial.read());
          dir *= DEG_TO_RAD;
          Serial.println(dir);
        }
        if (c == 'e') {
          while (Serial.available() < 1);
          sel = n(Serial.read());
        }
        if (c == 'p') {
          while (Serial.available() < 2);
          polygon = n(Serial.read())*10 + n(Serial.read());
          Serial.println(polygon);
        }
        if (c == 'l') {
          while (Serial.available() < 1);
          periodsPerLine = n(Serial.read());
          Serial.println(periodsPerLine);
        }
        
    }

    timer.run();
} 

int n(int c) {
  return c - 48;
}



void markTime() {
    period += STEP;
    if(period > 1) {
        periodReset = true;
        magn = magnRequest;
        period = 0. + 0.000001; 
//      selector = random(5);
        selector = sel;

        ++periodsElapsed;
        if (periodsElapsed >= periodsPerLine) {
            periodsElapsed = 0;
            dir += (2. * PI) / polygon;
            if (dir >= 2. * PI)
              dir = 0.;
        }
    }
}

void subsume() {
    walk.task(magn, dir, period);
    lean.task(magn, dir, period);
    idle.task(selector, period);    // default behaviour
    arbitrate();
}

void arbitrate() {
    if (periodReset) {
      periodReset = false;
      for(int i=0; i < 3; i++) {       // loop through tasks 
          if(behaviours[i] -> getFlag() == true) {    // check the flag
              winner = behaviours[i];                 // output a winner behaviour
              break;                                  // break at first enabled flag (subsume)
          }
      }
    }
    servos.writeAll(winner -> getPulses());         // the winner behaviour commands the servos 
}

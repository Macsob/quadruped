/* Arduino Parameters */
#define BAUDRATE 115200

#define PIN_SERVO_1   2
#define PIN_SERVO_2   3
#define PIN_SERVO_3   4
#define PIN_SERVO_4   5
#define PIN_SERVO_5   6
#define PIN_SERVO_6   7 
#define PIN_SERVO_7   8
#define PIN_SERVO_8   9
#define PIN_SERVO_9  10
#define PIN_SERVO_10 11
#define PIN_SERVO_11 12
#define PIN_SERVO_12 13

/* Global Parameters */
#define LEG1_ID 0
#define LEG2_ID 1
#define LEG3_ID 2
#define LEG4_ID 3

#define DEFAULT_X 130 //95
#define DEFAULT_Y 130 //95
#define DEFAULT_Z 115 //200 //80
#define LEG_Z_LIFT 40

#define NBR_SAMPLES    30    // number of samples to read 
#define NBR_AA    10    
#define WALK_MAXIMUM   150 //70    // 
#define WALK_THRESHOLD 45 //45    // minimum distance input value to start walking
#define LEAN_THRESHOLD  2    // minimum distance input value to start leaning forward
#define GAIT_AMPLITUDE 20

/* Robot Parameters */
#define NBR_SERVOS 12
#define NBR_LEGS 4
#define NBR_JOINTS 3        // joints per leg
#define CENTER_OFFSET 97.5    // distance from the center of robot and the first servo axisk
#define L1 27.5               // distance from the first servo axis and the second servo axis
#define L2 87             // distance from the second servo axis and the third servo axis
#define L3 130L//223L            // distance from the third servo axis and the tip of the leg

/* Time Parameters */
#define SPEED     15    // Movement speed
#define STEP  0.0025    // Movement fluidity(-), rigidity(+)


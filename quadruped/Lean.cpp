#include <Arduino.h>
#include <Wire.h>
#include "Behaviour.h"
#include "Calculator.h"
#include "Lean.h"
#include "Parameters.h"
 
Lean::Lean(): Behaviour() {}

void Lean::task(float magnitude_input, float direction_input, float period) {
    
    float magnitude = magnitude_input;//TODO
    float direction = direction_input;//TODO
    
    
    // Output control
    if(magnitude >= LEAN_THRESHOLD && magnitude < WALK_THRESHOLD) {
        center_x = magnitude*cos(direction) * (-cos(2*PI*period)+1)/2;
        center_y = magnitude*sin(direction) * (-cos(2*PI*period)+1)/2;
        center_z = 0;
        x[LEG1_ID] = DEFAULT_X;
        y[LEG1_ID] = DEFAULT_Y;
        z[LEG1_ID] = DEFAULT_Z;
        x[LEG2_ID] = -DEFAULT_X;
        y[LEG2_ID] = DEFAULT_Y;
        z[LEG2_ID] = DEFAULT_Z;
        x[LEG3_ID] = -DEFAULT_X;
        y[LEG3_ID] = -DEFAULT_Y;
        z[LEG3_ID] = DEFAULT_Z;
        x[LEG4_ID] = DEFAULT_X;
        y[LEG4_ID] = -DEFAULT_Y;
        z[LEG4_ID] = DEFAULT_Z;
        setFlag(true);
        
        Calculator::calculateAngles(getAngles(), center_x, center_y, center_z, x, y, z);

    }
    else {
        setFlag(false);
    }
    
}

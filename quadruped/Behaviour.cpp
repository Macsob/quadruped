#include <Arduino.h>
#include "Behaviour.h"
#include "Parameters.h"
#include "Calculator.h"
#include "Servos.h"

int Behaviour::count = 0;

Behaviour::Behaviour() {
    count++;
}

void Behaviour::setFlag(bool f) {
    _flag = f;
}

bool Behaviour::getFlag() {
    return _flag;
}

int* Behaviour::getPulses() {
    for(int i=0; i < NBR_LEGS; i++) {
        //alpha
        float alpha = _angles[NBR_JOINTS*i];
        alpha = constrain(alpha, 60, 120);
        //if (i == 1 || i == 2)
        alpha = 180. - alpha;
        _pulses[NBR_JOINTS*i] = Calculator::mapFloat(alpha, 0, 180, Servos::pwl[NBR_JOINTS*i], Servos::pwh[NBR_JOINTS*i]);

        //beta
        float beta = _angles[NBR_JOINTS*i+1];
        beta = beta < 125 ? beta : 125;
        if (i%2 == 1)
            beta = 180. - beta;
        _pulses[NBR_JOINTS*i+1] = Calculator::mapFloat(beta, 0, 180, Servos::pwl[NBR_JOINTS*i+1], Servos::pwh[NBR_JOINTS*i+1]);

        //gamma
        float gamma = _angles[NBR_JOINTS*i+2];
        gamma = gamma > 55 ? gamma : 55;
        if (i%2 == 0)
            gamma = 180. - gamma;
        _pulses[NBR_JOINTS*i+2] = Calculator::mapFloat(gamma, 0, 180, Servos::pwl[NBR_JOINTS*i+2], Servos::pwh[NBR_JOINTS*i+2]);
        
        
    }
    return _pulses;
}

float* Behaviour::getAngles() {
    return _angles;
}
